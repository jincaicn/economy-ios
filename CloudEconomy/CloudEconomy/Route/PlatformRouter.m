//
//  PlatformRouter.m
//  CloudEconomy
//
//  Created by 杜金彩 on 2019/12/4.
//  Copyright © 2019 djc. All rights reserved.
//

#import "PlatformRouter.h"
#import <flutter_boost/FlutterBoost.h>

@implementation PlatformRouter

+ (instancetype)sharedRouter {
    static PlatformRouter *sharedInstance=nil;
      static dispatch_once_t onceToken;
      dispatch_once(&onceToken, ^{
          sharedInstance = [[PlatformRouter alloc] init];
          [FlutterBoostPlugin.sharedInstance
                  startFlutterWithPlatform: sharedInstance
                                   onStart:^(FlutterEngine *engine) {
                                     NSLog(@"engine:%@", engine);
                                   }];
      });
      return sharedInstance;
}

- (void)open:(NSString *)name
   urlParams:(NSDictionary *)params
        exts:(NSDictionary *)exts
  completion:(void (^)(BOOL))completion
{
    BOOL animated = [exts[@"animated"] boolValue];
    FLBFlutterViewContainer *vc = FLBFlutterViewContainer.new;
    [vc setName:name params:params];
    [self.navigationController pushViewController:vc animated:animated];
    if(completion) completion(YES);
}

- (void)present:(NSString *)name
   urlParams:(NSDictionary *)params
        exts:(NSDictionary *)exts
  completion:(void (^)(BOOL))completion
{
    BOOL animated = [exts[@"animated"] boolValue];
    FLBFlutterViewContainer *vc = FLBFlutterViewContainer.new;
    [vc setName:name params:params];
    [self.navigationController presentViewController:vc animated:animated completion:^{
        if(completion) completion(YES);
    }];
}

- (void)close:(NSString *)uid
       result:(NSDictionary *)result
         exts:(NSDictionary *)exts
   completion:(void (^)(BOOL))completion
{
    BOOL animated = [exts[@"animated"] boolValue];
    animated = YES;
    FLBFlutterViewContainer *vc = (id)self.navigationController.presentedViewController;
    if([vc isKindOfClass:FLBFlutterViewContainer.class] && [vc.uniqueIDString isEqual: uid]){
        [vc dismissViewControllerAnimated:animated completion:^{}];
    }else{
        [self.navigationController popViewControllerAnimated:animated];
    }
}

@end
