//
//  AppDelegate.h
//  CloudEconomy
//
//  Created by 杜金彩 on 2019/12/2.
//  Copyright © 2019 djc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <flutter_boost/FlutterBoost.h>

@interface AppDelegate : FLBFlutterAppDelegate <UIApplicationDelegate>

@property(nonatomic,strong)UIWindow *window;
@end

