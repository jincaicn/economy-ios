//
//  AppDelegate.m
//  CloudEconomy
//
//  Created by 杜金彩 on 2019/12/2.
//  Copyright © 2019 djc. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"
#import "WebImage.h"
#import "PlatformRouter.h"
#import "MainTabBarViewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
     [PlatformRouter sharedRouter];
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = [MainTabBarViewController new];
    [self.window makeKeyAndVisible];
   
//    [WebImage loadWebpImage];
    return YES;
}

@end
