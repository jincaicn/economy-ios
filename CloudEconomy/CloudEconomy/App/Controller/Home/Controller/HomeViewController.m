//
//  HomeViewController.m
//  CloudEconomy
//
//  Created by 杜金彩 on 2019/12/4.
//  Copyright © 2019 djc. All rights reserved.
//

#import "HomeViewController.h"
@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor = [UIColor redColor];
    btn.frame = CGRectMake(100, 100, 150, 50);
    [btn setTitle:@"跳转flutter" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(pushFlutter) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    
}
- (void)pushFlutter{

}


@end
