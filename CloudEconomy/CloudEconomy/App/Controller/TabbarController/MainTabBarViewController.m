//
//  MainTabBarViewController.m
//  CloudEconomy
//
//  Created by 杜金彩 on 2019/12/4.
//  Copyright © 2019 djc. All rights reserved.
//

#import "MainTabBarViewController.h"
#import "HomeViewController.h"
#import "MyViewController.h"
#import <flutter_boost/FlutterBoost.h>

static NSString *const homeTabbarTitle = @"花钱";
static NSString *const orderTabbarTitle = @"订单";
static NSString *const purseTabbarTitle = @"赚钱";
static NSString *const cartTabbarTitle = @"购物车";
static NSString *const myTabbarTitle = @"我的";

@interface MainTabBarViewController ()<UITabBarControllerDelegate>

@end

@implementation MainTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tabBar.backgroundColor =  [UIColor whiteColor];
     self.tabBar.translucent = NO;
     self.delegate = self;
    
        HomeViewController *home = [[HomeViewController alloc]init];
        [self addChildViewController:home title:homeTabbarTitle image:@"index_tab_home_normal" selectedImage:@"index_tab_home_in"];

        FLBFlutterViewContainer *fvc = FLBFlutterViewContainer.new;
        [self addChildViewController:fvc title:@"小淘鸡" image:@"" selectedImage:@""];
        
        MyViewController *my = [[MyViewController alloc]init];
        [self addChildViewController:my title:myTabbarTitle image:@"index_tab_account_normal" selectedImage:@"index_tab_account_in"];
}


- (void)addChildViewController:(UIViewController *)childVC title:(NSString *)title image:(NSString *)imageName selectedImage:(NSString *)selectedImageName
{
    // 1.设置控制器的属性
    childVC.title = title;
    // 设置图标
    childVC.tabBarItem.image = [UIImage imageNamed:imageName];
    // 设置选中的图标
    UIImage *selectedImage = [UIImage imageNamed:selectedImageName];
    
    childVC.tabBarItem.selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [childVC.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:RGBCOLOR(245, 245, 245)} forState:UIControlStateNormal];
    [childVC.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:RGBCOLOR(238, 59, 72)} forState:UIControlStateSelected];
    
    // 2.包装一个导航控制器
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:childVC];
    [self addChildViewController:nav];
}

@end
