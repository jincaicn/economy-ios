//
//  Contant.h
//  HZPurse
//
//  Created by huazhuan on 2017/8/9.
//  Copyright © 2017年 huazhuan. All rights reserved.
//

#ifndef Contant_h
#define Contant_h


#ifdef DEBUG
# define CELog(fmt, ...) NSLog((@"[文件名:%s]\n" "[函数名:%s]\n" "[行号:%d] \n" fmt), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
# define CELog(...);
#endif




/*******自定义的一些可能会用到的宏*********/
//TODO

#define NATIVE_ENTRYARRAYS  @[NATIVE_ENTRY_SEARCH,NATIVE_ENTRY_CHECKIN]

#define NSLocalizeString(key)  [[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:@"HZPurseLocalizable"] //读取本地化文件

#define PasteboardString [UIPasteboard generalPasteboard].string  //粘贴板信息

#define WeakSelf __weak typeof(self) weakSelf=self
#define StrongSelf __strong typeof(weakSelf) strongSelf=weakSelf;

#define DEGREESTO_RADIANS(angle) ((angle) / 180.0 * M_PI)
#define RADIANS_TODEGREES(radians) ((radians) * (180.0 / M_PI))



#define DESCRIPTION_COLOR RGBCOLOR(255, 207, 192)   // 描述文本颜色
#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
#define RGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

#define ColorWithBackgroundColor UIColorFromRGB(0xf5f5f5)  //灰色
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define UIColorWithRGBAlpha(rgbValue,a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

#define APP_DOCUMENT_PATH [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]
#define APP_TEMP_PATH [NSHomeDirectory() stringByAppendingPathComponent:@"temp"]
#define APP_LIBRARY_PATH [NSHomeDirectory() stringByAppendingPathComponent:@"Library"]
#define APP_LIBRARY_CACHES_PATH [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject]

#define IsIOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define IsIOS9 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)
#define IsIOS10 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)
#define IsIOS11 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0)

#define Is_Iphone_5s ([[UIScreen mainScreen] bounds].size.height == 568.0f)
#define Is_Iphone_6 ([[UIScreen mainScreen] bounds].size.height == 667.0f)
#define Is_Iphone_6P ([[UIScreen mainScreen] bounds].size.height == 736.0f)
#define Is_Iphone_X  ([[UIScreen mainScreen] bounds].size.height == 812.0f)  // iPhonex  5.8寸   375*812

#define BOTTOM Is_Iphone_X ? -54 : 0

#define StatusHeight [UIApplication sharedApplication].statusBarFrame.size.height
#define kScreenWidth [[UIScreen mainScreen] bounds].size.width
#define kScreenHeight [[UIScreen mainScreen] bounds].size.height
#define APPStatusHeight (Is_Iphone_X ? 44 : 20)
#define APPNaviHeight 44
#define APPTopHeight (APPStatusHeight + APPNaviHeight) //导航栏 + 状态栏高度
#define APPTabbarHeight (Is_Iphone_X ? 83 : 49)
#define RefreshHeight 50  //刷新的背景颜色高度
#define PrestrainNumber 5  //滑到第五个预加载数据
#define TopBtnHiddenHeight 300 //顶部按钮显示的高度
#define BottomHeight (Is_Iphone_X ? 20 : 0)
#define imageFloadHeight 80 //浮窗宽度
#define ImageOffset_x 30 //偏移
#define ViewSize(size)  (kScreenWidth * size / 750.0) //视图等比放缩 所有尺寸都是基于iPhone6s尺寸

#define DEFAULT_IMAGE [UIImage imageNamed:@"icon_default"]

#define OneTimeDay  (60 * 60 * 24)  //一天时间

#define RepetClickCount 1  //按钮1秒内不重复点击

#define TBShopType @[@"1",@"2",@"3"]

#define NewHeight 40 //消息高度

#define AliPlaySuccessCode 9000

#define SMSSDK_FREQ_LIMIT_ERROR_CODES @[@300472, @300477, @300476, @300478, @300463, @300462, @300464, @300465]

#endif /* Contant_h */
