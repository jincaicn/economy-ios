//
//  Env.m
//  CloudEconomy
//
//  Created by 杜金彩 on 2019/12/3.
//  Copyright © 2019 djc. All rights reserved.
//

#import "Env.h"
//#import "DeviceInfoManager.h"
//#import "KeyChainManager.h"

#define UserIdKey @"userInfoId"       //userid保存到本地key
#define UserTokenIdKey @"userTokenId" //usertoken保存到本地key
#define APIHostKey @"APIHostKey"      //API地址

@implementation Env
//+ (NSArray *)getHostApiList {
//    return @[
//        @"https://api.huazhuanapp.com",
//        @"https://api.huazhuanapp.com",
//        @"https://api.huazhuanapp.com",
//    ];
//}
//
//+ (NSString *)getDeviceChareKey {
//    NSArray *hostArray = [self getHostApiList];
//    NSString *apiNum = [[NSUserDefaults standardUserDefaults] objectForKey:APIHostKey];
//
//    if ([NSString isEqualEmpty:apiNum]) {
//        return hostArray[0];
//    }
//    return hostArray[[apiNum intValue]] ;
//}
//
//+ (NSString *)getAPIHost {
//    NSArray *hostArray = [self getHostApiList];
//    NSString *apiNum = [[NSUserDefaults standardUserDefaults] objectForKey:APIHostKey];
//    if ([NSString isEqualEmpty:apiNum]) {
//        return hostArray[0];
//    }
//    return hostArray[[apiNum intValue]];
//}
//+ (NSString *)getAppCI {
//    NSString *ci = [KeyChainManager openUUID];
//    if ([NSString isEqualEmpty:ci]) {
//        return @"";
//    }
//    return ci;
//}
//
//+ (NSString *)getAppCP {
//    return @"i";
//}
//
//+ (NSString *)getAppCV {
//    return [[DeviceInfoManager sharedManager] getAppVersion];
//}
//
//+ (NSString *)getDeviceId {
//    NSString *key = [NSString stringWithFormat:@"deviceId%@", [self getDeviceChareKey]];
//    NSString *deviceId = (NSString *)[KeyChainManager load: key];
//    if ([NSString isEqualEmpty:deviceId]) {
//        return @"";
//    }
//    return deviceId;
//
//}
//
//+ (NSString *)getDeviceKey {
//    NSString *key = [NSString stringWithFormat:@"deviceId%@", [self getDeviceChareKey]];
//    NSString *deviceKey = (NSString *)[KeyChainManager load: key];
//    if ([NSString isEqualEmpty:deviceKey]) {
//        return @"";
//    }
//    return deviceKey;
//
//}
//+ (NSString *)getUserId {
//    NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:UserIdKey];
//    if ([NSString isEqualEmpty:userId]) {
//        return @"";
//    }
//    return userId;
//}
//+ (NSString *)getUserToken {
//    NSString *userToken = [[NSUserDefaults standardUserDefaults] objectForKey:UserTokenIdKey];
//    if ([NSString isEqualEmpty:userToken]) {
//        return @"";
//    }
//    return userToken;
//}
@end
