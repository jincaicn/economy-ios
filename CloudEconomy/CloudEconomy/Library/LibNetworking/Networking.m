//
//  Networking.m
//  CloudEconomy
//
//  Created by 杜金彩 on 2019/12/3.
//  Copyright © 2019 djc. All rights reserved.
//

#import "Networking.h"
//#import <AFNetworking/AFNetworking.h>
#import "Env.h"

@implementation Networking

//+ (void)get:(NSString *)url params:(NSDictionary *)params success:(void (^)(id))success failure:(void (^)(NSError *))failure {
//    NSDictionary *dict = [self getParamsWith:params];
//
//    NSString *newURL = [NSString stringWithFormat:@"%@%@", [Env getAPIHost], url];
//
//    CELog(@"GET请求的地址为：%@", [newURL paramsStringWithParams:dict]);
//    //1.获得请求管理者
//    AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
//
//    //2.发送Get请求
//    [mgr GET:newURL
//        parameters:dict
//        progress:nil
//        success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
//          CELog(@"GET请求数据：%@", responseObject);
//          if (success) {
//              success(responseObject);
//          }
//        }
//        failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
//          CELog(@"GET请求错误：%@", error);
//          if (failure) {
//              failure(error);
//          }
//        }];
//}
//
//+ (NSURLSessionDataTask *)post:(NSString *)url params:(NSDictionary *)params success:(void (^)(id))success failure:(void (^)(NSError *))failure {
//    NSDictionary *dict = [self getParamsWith:params];
//    NSString *newURL = [NSString stringWithFormat:@"%@%@", [Env getAPIHost], url];
//    CELog(@"POST请求的地址为：%@", [newURL paramsStringWithParams:dict]);
//
//    //1.获得请求管理者
//    AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
//
//    NSURLSessionDataTask *dataTask = [mgr POST:newURL
//        parameters:dict
//        progress:nil
//        success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
//          CELog(@"POST请求数据：%@", responseObject);
//          if (success) {
//              success(responseObject);
//          }
//
//        }
//        failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
//          CELog(@"POST请求错误：%@", error);
//          if (failure) {
//              failure(error);
//          }
//        }];
//
//    return dataTask;
//}
//
//+ (NSDictionary *)getParamsWith:(NSDictionary *)params {
//    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//    [dict setObject:[Env getAppCP] forKey:@"cp"];
//    [dict setObject:[Env getAppCV] forKey:@"cv"];
//    [dict setObject:[Env getAppCI] forKey:@"ci"];
//    [dict setObject:[Env getUserId] forKey:@"uid"];
//    [dict setObject:[Env getUserToken] forKey:@"token"];
//    [dict addEntriesFromDictionary:params];
//
//    return [NSDictionary dictionaryWithDictionary:dict];
//}



@end
