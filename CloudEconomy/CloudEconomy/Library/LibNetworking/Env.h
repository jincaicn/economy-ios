//
//  Env.h
//  CloudEconomy
//
//  Created by 杜金彩 on 2019/12/3.
//  Copyright © 2019 djc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Env : NSObject

+ (NSArray *)getHostApiList;
+ (NSString *)getAPIHost;
+ (NSString *)getAppCP;
+ (NSString *)getAppCV;
+ (NSString *)getAppCI;
+ (NSString *)getDeviceId;
+ (NSString *)getDeviceKey;
+ (NSString *)getUserId;
+ (NSString *)getUserToken;

@end

NS_ASSUME_NONNULL_END
