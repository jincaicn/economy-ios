//
//  KeyChainManager.h
//  HZPurse
//
//  Created by huazhuan on 2017/8/31.
//  Copyright © 2017年 huazhuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeyChainManager : NSObject

+ (id)load:(NSString *)service;
+ (NSString *)openUUID;

@end
