//
//  NSString+RegularExpression.h
//  HZPurse
//
//  Created by huazhuan on 2017/8/16.
//  Copyright © 2017年 huazhuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (RegularExpression)
//检查url连接匹配
- (BOOL)checkWebURLWith:(NSString *)urlString;

//检查邮箱是否正确
- (BOOL)checkEmailText;
//检查手机号码是否正确
- (BOOL)checkPhoneText;

//检查淘宝链接
- (BOOL)checkTaobaoURL;

//检查webp图片链接
- (BOOL)checkImageUrl;

@end
