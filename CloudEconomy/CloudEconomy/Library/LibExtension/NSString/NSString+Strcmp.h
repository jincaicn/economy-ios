//
//  NSString+Strcmp.h
//  CloudEconomy
//
//  Created by 杜金彩 on 2019/12/3.
//  Copyright © 2019 djc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Strcmp)

/// 拼接z数据
/// @param dict 返回拼接后字符串
- (NSString *)paramsStringWithParams:(NSDictionary *)dict;
/// 处理数字字符串加逗号 返回123,524
/// @param str @"123524"
+ (NSString *)strmethodComma:(NSString *)str;

/// 替换字符串
- (NSString *)replaceCharacterToEmpty;

///判断字符串是否为空
+ (BOOL)isEqualEmpty:(NSString *)str;


@end

NS_ASSUME_NONNULL_END
