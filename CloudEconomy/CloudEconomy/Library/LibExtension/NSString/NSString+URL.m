//
//  NSString+URL.m
//  HZPurse
//
//  Created by huazhuan on 2017/8/22.
//  Copyright © 2017年 huazhuan. All rights reserved.
//

#import "NSString+URL.h"

@implementation NSString (URL)

/**
 *  URLEncode
 */
- (NSString *)URLEncodedString
{
    NSString *encodedString = (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                              (CFStringRef)self,
                                                              NULL,
                                                              (CFStringRef)@"!$&'()*+,-./:;=?@_~%#[]",
                                                              kCFStringEncodingUTF8));
    return encodedString;
}

/**
 *  URLDecode
 */
-(NSString *)URLDecodedString
{
    //NSString *decodedString = [encodedString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
    NSString *encodedString = self;
    NSString *decodedString  = (__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault,(__bridge CFStringRef)encodedString,CFSTR(""),CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
    return decodedString;
}


/**
 *  截取URL中的参数
 *
 *  @return NSMutableDictionary parameters
 */
- (NSMutableDictionary *)getURLParameters
{
    
    // 查找参数
    NSRange range = [self rangeOfString:@"?"];
    if (range.location == NSNotFound) {
        return nil;
    }
    
    // 以字典形式将参数返回
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    // 截取参数
    NSString *parametersString = [self substringFromIndex:range.location + 1];
    
    // 判断参数是单个参数还是多个参数
    if ([parametersString containsString:@"&"]) {
        
        // 多个参数，分割参数
        NSArray *urlComponents = [parametersString componentsSeparatedByString:@"&"];
        
        for (NSString *keyValuePair in urlComponents) {
            // 生成Key/Value
            NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
            NSString *key = pairComponents.firstObject;
            NSString *value = pairComponents.lastObject;
            
            // Key不能为nil
            if (key == nil || value == nil) {
                continue;
            }
            
            // 设置值，忽略重复值
            [params setValue:value forKey:key];
            
            /*
            id existValue = [params valueForKey:key];
            
            if (existValue != nil) {
                
                // 已存在的值，生成数组
                if ([existValue isKindOfClass:[NSArray class]]) {
                    // 已存在的值生成数组
                    NSMutableArray *items = [NSMutableArray arrayWithArray:existValue];
                    [items addObject:value];
                    
                    [params setValue:items forKey:key];
                } else {
                    
                    // 非数组
                    [params setValue:@[existValue, value] forKey:key];
                }
                
            } else {
                
                // 设置值
                [params setValue:value forKey:key];
            }
             */
        }
    } else {
        // 单个参数
        
        // 生成Key/Value
        NSArray *pairComponents = [parametersString componentsSeparatedByString:@"="];
        
        // 只有一个参数，没有值
        if (pairComponents.count == 1) {
            return nil;
        }
        
        // 分隔值
        NSString *key = pairComponents.firstObject;
        NSString *value = pairComponents.lastObject;
        
        // Key不能为nil
        if (key == nil || value == nil) {
            return nil;
        }
        
        // 设置值
        [params setValue:value forKey:key];
    }
    
    return params;
}

/**
 *  截取URL中的域名
 *
 *  @return NSString str
 */
- (NSString *)getURDomain
{
    NSArray *domainArray = [self componentsSeparatedByString:@"?"];

    NSString *str = domainArray[0];
    
    return str;
    
}
/**
 *  URL链接
 *
 *  @return NSString str
 */
- (NSString *)getURLWith:(NSDictionary *)params
{
    if (!params.allKeys.count) {
        return self;
    }
    
    NSMutableString *paramsStr = [[NSMutableString alloc]init];
    [params enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if (paramsStr.length == 0) {
            [paramsStr appendString:[NSString stringWithFormat:@"%@=%@",key,obj]];
        } else {
            [paramsStr appendString:[NSString stringWithFormat:@"&%@=%@",key,obj]];
        }
    }];
    
    return [NSString stringWithFormat:@"%@?%@", self, paramsStr];
}

@end
