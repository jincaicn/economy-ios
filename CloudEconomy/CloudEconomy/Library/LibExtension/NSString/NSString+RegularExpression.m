//
//  NSString+RegularExpression.m
//  HZPurse
//
//  Created by huazhuan on 2017/8/16.
//  Copyright © 2017年 huazhuan. All rights reserved.
//

#import "NSString+RegularExpression.h"

@implementation NSString (RegularExpression)

//检查url连接匹配
- (BOOL)checkWebURLWith:(NSString *)urlString
{
   // NSString *phoneRegex = @"^1[3,4,5,7,8]{1}\\d{9}$";
    
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",urlString];
    return [phoneTest evaluateWithObject:self];
    
}


//检查邮箱是否正确
- (BOOL)checkEmailText
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

//检查手机号码是否正确
- (BOOL)checkPhoneText
{
    //只判断首位是1的11位数字
    NSString *phoneRegex = @"^1[0-9]{10}$";//  ^1[3,4,5,7,8]{1}\\d{9}$
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:self];
}

//检查淘宝链接
- (BOOL)checkTaobaoURL
{
    NSString* valregex = @"^(tbopen|tmall)://.*";
    NSPredicate* valtest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", valregex];
    return [valtest evaluateWithObject:self];
}


- (BOOL)checkImageUrl
{
    NSString * tbImgPattern = @"^https?://img\\.alicdn\\.com.*\\.jpg$";
    if (![[NSPredicate predicateWithFormat:@"SELF MATCHES %@", tbImgPattern] evaluateWithObject:self]) {
        return YES;
    }
    
    NSString* valregex = @"^.*(_\\d+x\\d+[^.]+\\.jpg.*|png|webp)$";
    NSPredicate* valtest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", valregex];
    return [valtest evaluateWithObject:self];
}


@end
