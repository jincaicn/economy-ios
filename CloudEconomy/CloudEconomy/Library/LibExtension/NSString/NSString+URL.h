//
//  NSString+URL.h
//  HZPurse
//
//  Created by huazhuan on 2017/8/22.
//  Copyright © 2017年 huazhuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (URL)

/**
 *  URLEncode
 */
- (NSString *)URLEncodedString;

/**
 *  URLDecode
 */
-(NSString *)URLDecodedString;

/**
 *  截取URL中的参数
 *
 *  @return NSMutableDictionary parameters
 */
- (NSMutableDictionary *)getURLParameters;

/**
 *  截取URL中的域名
 *
 *  @return NSString str
 */
- (NSString *)getURDomain;

/**
 *  URL链接
 *
 *  @return NSString str
 */
- (NSString *)getURLWith:(NSDictionary *)params;

@end
