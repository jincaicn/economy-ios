//
//  NSString+Strcmp.m
//  CloudEconomy
//
//  Created by 杜金彩 on 2019/12/3.
//  Copyright © 2019 djc. All rights reserved.
//

#import "NSString+Strcmp.h"

@implementation NSString (Strcmp)

+ (BOOL)isEqualEmpty:(NSString *)str {
    
    if (!str) {
        return YES;
    }
    if ([str isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if (!str.length) {
        return YES;
    }
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmedStr = [str stringByTrimmingCharactersInSet:set];
    if (!trimmedStr.length) {
        return YES;
    }
    return NO;
}

- (NSString *)paramsStringWithParams:(NSDictionary *)dict {
    __block NSMutableString *string = [NSMutableString stringWithString:self];

    if (dict.allKeys.count) {
        [string appendString:@"?"];
    }

    [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
      if ([string hasSuffix:@"?"]) {
          [string appendString:[NSString stringWithFormat:@"%@=%@", key, obj]];
      } else {
          [string appendString:[NSString stringWithFormat:@"&%@=%@", key, obj]];
      }
    }];

    return [NSString stringWithString:string];
}
+ (NSString *)strmethodComma:(NSString *)str {
    NSString *intStr;
    NSString *floStr;

    if ([str containsString:@"."]) {
        NSRange range = [str rangeOfString:@"."];

        floStr = [str substringFromIndex:range.location];

        intStr = [str substringToIndex:range.location];

    } else {
        floStr = @"";

        intStr = str;
    }

    if (intStr.length <= 3) {
        return [intStr stringByAppendingString:floStr];

    } else {
        NSInteger length = intStr.length;

        NSInteger count = length / 3;

        NSInteger y = length % 3;

        NSString *tit = [intStr substringToIndex:y];

        NSMutableString *det = [[intStr substringFromIndex:y] mutableCopy];

        for (int i = 0; i < count; i++) {
            NSInteger index = i + i * 3;

            [det insertString:@"," atIndex:index];
        }

        if (y == 0) {
            det = [[det substringFromIndex:1] mutableCopy];
        }

        intStr = [tit stringByAppendingString:det];

        return [intStr stringByAppendingString:floStr];
    }
}

- (NSString *)replaceCharacterToEmpty {
    NSString *str;
    str = [self stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    return self;
}
@end
