//
//  WebImage.h
//  CloudEconomy
//
//  Created by 杜金彩 on 2019/12/4.
//  Copyright © 2019 djc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//#import <SDWebImage/SDWebImage.h>

NS_ASSUME_NONNULL_BEGIN

@interface WebImage : NSObject

/// 加载webpimage
//+ (void)loadWebpImage;
//
//+ (void)setImageWith:(nullable UIImageView *)imageView imageUrl:(nullable NSString *)imgUrl;
//
//+ (void)setImageWith:(nullable UIImageView *)imageView imageUrl:(nullable NSString *)imgUrl placeholderImage:(nullable UIImage *)placeholder;
//
//+ (void)setImageWith:(nullable UIImageView *)imageView imageUrl:(nullable NSString *)imgUrl placeholderImage:(nullable UIImage *)placeholder completed:(nullable SDExternalCompletionBlock)completedBlock;
//
//+ (void)setImageWith:(nullable UIImageView *)imageView imageUrl:(nullable NSString *)imgUrl placeholderImage:(nullable UIImage *)placeholder options:(SDWebImageOptions)options completed:(nullable SDExternalCompletionBlock)completedBlock;
//
//+ (NSString *)readWebImageCache;
//
//+ (void)clearWebImageCache:(nullable SDWebImageNoParamsBlock)completion;

@end

NS_ASSUME_NONNULL_END
