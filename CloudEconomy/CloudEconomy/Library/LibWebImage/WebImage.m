//
//  WebImage.m
//  CloudEconomy
//
//  Created by 杜金彩 on 2019/12/4.
//  Copyright © 2019 djc. All rights reserved.
//

#import "WebImage.h"
//#import <SDWebImage/SDWebImage.h>
//#import <SDImageCache.h>
//#import <SDWebImageWebPCoder/SDWebImageWebPCoder.h>

@implementation WebImage

//+ (void)loadWebpImage {
//    SDImageWebPCoder *webPCoder = [SDImageWebPCoder sharedCoder];
//    [[SDImageCodersManager sharedManager] addCoder:webPCoder];
//}
//
//+ (void)setImageWith:(UIImageView *)imageView imageUrl:(NSString *)imgUrl{
//   [imageView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
//}
//
//+ (void)setImageWith:(UIImageView *)imageView imageUrl:(NSString *)imgUrl placeholderImage:(UIImage *)placeholder {
//    [imageView sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:placeholder];
//}
//
//+ (void)setImageWith:(UIImageView *)imageView imageUrl:(NSString *)imgUrl placeholderImage:(UIImage *)placeholder completed:(SDExternalCompletionBlock)completedBlock {
//    [imageView sd_setImageWithURL:[NSURL URLWithString: imgUrl] placeholderImage:placeholder completed:completedBlock];
//}
//
//+ (void)setImageWith:(UIImageView *)imageView imageUrl:(NSString *)imgUrl placeholderImage:(UIImage *)placeholder options:(SDWebImageOptions)options completed:(SDExternalCompletionBlock)completedBlock {
//    [imageView sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:placeholder options:options completed:completedBlock];
//}
//
//+ (NSString *)readWebImageCache  {
//    NSUInteger size = [SDImageCache sharedImageCache].totalDiskSize;
//    // 1k = 1024, 1m = 1024k
//    if (size < 1024) { // 小于1k
//        return [NSString stringWithFormat:@"%ldB",(long)size];
//    }else if (size < 1024 * 1024) { // 小于1m
//        CGFloat aFloat = size/1024;
//        return [NSString stringWithFormat:@"%.0fK",aFloat];
//    }else if (size < 1024 * 1024 * 1024) { // 小于1G
//        CGFloat aFloat = size/(1024 * 1024);
//        return [NSString stringWithFormat:@"%.1fM",aFloat];
//    }else {
//        CGFloat aFloat = size/(1024*1024*1024);
//        return [NSString stringWithFormat:@"%.1fG",aFloat];
//    }
//}
//
//+ (void)clearWebImageCache:(SDWebImageNoParamsBlock)completion {
//    [[SDImageCache sharedImageCache] clearDiskOnCompletion:completion];
//}

@end
